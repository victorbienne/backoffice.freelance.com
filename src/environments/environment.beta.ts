export const environment = {
  production: false,
  debug: true,
  // api: {
  //   url:		'https://beta.api.consultant.freelance.com',
  //   filePath:		'https://beta.api.consultant.freelance.com/uploads/profiles',
  // },
  api: {
    url:		'https://beta.api.entreprise.freelance.com',
    filePath:		'https://beta.api.entreprise.freelance.com/uploads/profiles',
  },
  // domain: 'beta.Backoffice.freelance.com'   // relax domain for inter-communication with iframe
  domain: 'www.adm-system.com'
};
