export const environment = {
  production: false,
  debug: true,
  api: {
    url:		'http://127.0.0.1:8000',
    filePath:		'http://127.0.0.1:8000/uploads/profiles',
  },
  // api: {
  //   url:		'http://localhost:8000',
  //   filePath:		'http://localhost:8000/uploads/profiles',
  // },
  // domain: 'test.espaceconsultant.freelance.com',   // relax domain for inter-communication with iframe
  domain: 'www.adm-system.com',
};
