export const environment = {
  production: true,
  // api: {
  //   url:		'https://api.consultant.freelance.com',
  //   filePath:		'https://api.consultant.freelance.com/uploads/profiles',
  // },
  api: {
    url:		'https://api.entreprise.freelance.com',
    filePath:		'https://api.entreprise.freelance.com/uploads/profiles',
  },
  domain: 'Backoffice.freelance.com'   // relax domain for inter-communication with iframe
};
