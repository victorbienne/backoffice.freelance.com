import { Http } from '@angular/http';
import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, Input, EventEmitter, Output } from '@angular/core';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { environment } from '../../../environments/environment';

declare var jquery: any;
declare var $: any;

// const ADM_SYSTEM_URL         = 'http://test.espaceconsultant.freelance.com';
const ADM_SYSTEM_URL         = 'http://admsystem';
const CSS_CLASSES_TO_KEEP    = '.cadre, .cadre0, .cadre1, .cadre2, .pageTab' +
                               ', #leftCol, #rightCol, .top, .in';             // for Events page
const MIN_POLLING_INTERVAL   = 300;  // cannot poll below this threshold!!
const FALLBACK_DISPLAY_DELAY = 2500;
const DEACTIVATE_AUTORESIZE  = -42;

@Component({
    selector: 'app-iframe-legacy',
    templateUrl: './iframe-legacy.component.html',
    styleUrls: ['./iframe-legacy.component.sass']
})
export class IframeLegacyComponent implements OnInit {
    @Input() path: string;
    @Output() onContentUnload = new EventEmitter<boolean>();
    @Output() onContentResized = new EventEmitter<number>();

    public env: any = environment;
    public src: SafeResourceUrl;
    public style: any = {
        display: 'none',
        height: '100%'
    };
    public $iframe = {  // all attributes are jQuery objects
        root    : null,
        window  : null,
        head    : null,
        document: null,
        body    : null,
        _status : 'hidden',
        _display: () => {
            this.style.display = (this.$iframe._status !== 'hidden') ? 'block' : 'none';
        },
        show    : (status?: string) => {
            this.$iframe._status = (status) ? status : 'shown';
            this.$iframe._display();
        },
        hide    : ()                => {
            this.$iframe._status = (this.$iframe._status === 'forced') ? 'forced' : 'hidden';
            this.$iframe._display();
        },
    }
    private _timer = {
        cssInjection   : null,
        height         : null,
        fallbackDisplay: null
    }

    @ViewChild('iframe') iframe: ElementRef;

    constructor(
        public http: Http,
        private sanitizer: DomSanitizer) {
    }

    ngOnInit() {
        this.$iframe.root = $('#iframe');
        this._setupIframe(this.$iframe.root);
    }

    // ---------------------
    //   IFrame own events
    // ---------------------
    public onLoad(event) {
        // console.log(event);
    }
    public onError(event) {
        // console.log(event);
    }

    // -----------------------
    //   Debug-usage methods
    // -----------------------
    public getInternalStatus() {
        return this.$iframe._status;
    }

    public toggleStatus() {
        const statuses = ['hidden', 'shown', 'forced'];
        const newStatus = (statuses.findIndex((entry) => entry === this.$iframe._status) + 1) % statuses.length;

        this.$iframe.show(statuses[newStatus]);
    }

    public toggleAutoresize() {
        if (this._timer.height &&
            this._timer.height !== DEACTIVATE_AUTORESIZE) {   // autoresize is ON
            clearInterval(this._timer.height);
            this._timer.height = DEACTIVATE_AUTORESIZE;
        } else {                    // autoresize is OFF
            this._timer.height = null;  // allow reactivation
            this._autoResize($('html', this.$iframe.document)[0]);
        }
    }

    // -----------------------
    //   CSS-related methods
    // -----------------------

    // Whenever navigation within the IFrame document occurs, we would
    // like to be able to react to this as needed
    private _handleInternalNavigation(iframeWindow) {
        iframeWindow.on('unload', function($event) {
            postMessage({ message: 'IFRAME_UNLOAD' }, '*');
        });
    }

    // Watch the given element - every frequency ms - for change in height
    // and resize the iframe accordingly
    //
    // Remarks
    // -------
    //   there is a need to do this with POLLING since any action of even
    //   hovering an element might result in changing height of the
    //   document
    //   Didn't manage to have approaches such as ResizeObserver to work
    private _autoResize(watchedElement, frequency: number = 500) {
        if (this._timer.height === DEACTIVATE_AUTORESIZE) { return; }

        const that         = this;
        const $iframeStruc = this.$iframe;

        this._timer.height = setInterval(() => {
            try {
                let height = getComputedStyle(watchedElement, null).getPropertyValue('height');
                const h: number = parseInt(height.replace(/\D/g, ''), 10);

                height = (h < 80) ? '100%' : height;
                that.style.height = height;
            } catch (e) {
                // console.error(e);
                that.style.height = '100%';
            }
            this.onContentResized.emit(that.style.height);
        }, Math.max(frequency, MIN_POLLING_INTERVAL));
    }

    private _injectCSS(iframeDocument) {
        $('head', iframeDocument).append($('<link/>', {
            rel: 'stylesheet',
            href: document.location.origin + '/assets/hybrid/css/hybrid.css',  // need absolute URL from the PARENT
            type: 'text/css',
            onload: ((iframeDoc, timer) => {
                // Check for correct rendering of new CSS
                timer.cssInjection = setInterval(() => {
                    /* Magic number rgba(0,173,239,0) is forced through CSS and we simply poll for correct application */
                    if ($('body', iframeDoc).css('border-bottom-color').replace(/\s/g, '') === 'rgba(0,173,239,0)') {
                        clearInterval(timer.cssInjection);
                        timer.cssInjection = null;
                        postMessage({ message: 'CSS_FOR_IFRAME_LOADED' }, '*');
                    }
                }, MIN_POLLING_INTERVAL);
            })(iframeDocument, this._timer)  // IIFE trick
        }));
    }

    private _setupIframe($iframe) {
        const origin = window.location.origin;
        // console.log('** Setuping iframe... **');

        // For Angular to allow the dynamically-built URL to be displayed correctly
        if (this.path.indexOf('http') >= 0) {
            this.src = this.sanitizer.bypassSecurityTrustResourceUrl(this.path);
        } else {
            this.src = this.sanitizer.bypassSecurityTrustResourceUrl(ADM_SYSTEM_URL + '' + this.path);
        }

        // -----------------------
        //   Messaging to parent
        // -----------------------
        // Either:
        //  1) wait for the iframe to acknowledge loading of external CSS
        //  2) set a timer (in case domain do not match OR event got lost)
        // in order to avoid Flash Of Unstyled Content (FOUC)
        // $iframe.on('message', this._handleMessage(this, new Date()));
        $(window).off('message');
        $(window).on('message', (event) => {
            if (!(event.type === 'message'))         { return; }
            if (!(event.originalEvent.data &&
                  event.originalEvent.data.message)) { return; }

            switch (event.originalEvent.data.message) {
                case 'CSS_FOR_IFRAME_LOADED':
                    this.$iframe.show();
                    // console.log('CSS loaded', new Date());
                    break;
                case 'IFRAME_UNLOAD':
                    this.$iframe.hide();
                    clearInterval(this._timer.height);
                    this._timer.height = null;
                    // console.log('Unload', new Date());

                    // Fallback in case LOAD event is not trigerred (i.e. PDF document on IE in another domain)
                    clearTimeout(this._timer.fallbackDisplay);
                    this._timer.fallbackDisplay = setTimeout(() => { this.$iframe.show(); }, FALLBACK_DISPLAY_DELAY);

                    this.onContentUnload.emit(true);
                    break;
            }
        });

        // -----------
        //   Styling
        // -----------
        // Whenever navigation occurs within the iframe, the 'src' attribute does NOT
        // change but the onload event is still triggerred
        $iframe.on('load', () => {
            // --------------------
            //   Error management
            // --------------------
            try {
                $iframe.contents();

                // Keep hold of IFrame elements (this may trigger an error on Firefox)
                this.$iframe.window   = $($iframe[0].contentWindow);
                this.$iframe.document = $iframe.contents();
                this.$iframe.head     = $('head', this.$iframe.document);
                this.$iframe.body     = $('body', this.$iframe.document);
            } catch (e) {
                // const iframe: HTMLIFrameElement = document.getElementById('iframe') as HTMLIFrameElement;
                // const iframeDefaultDomain = iframe.contentDocument.domain;
                const iframeDefaultDomain = $iframe.attr('src').split('://')[1].split('/')[0];

                // Avoid blank page
                this.$iframe.show('forced');
                this.style.height = '100%';

                // Listen for content resize and notifies
                this._autoResize($('html', this.$iframe.document)[0]);

                const domainSpace  = document.domain.split('.');
                const domainOther  = new Set(iframeDefaultDomain.split('.'));
                const intersection = domainSpace.filter(x => domainOther.has(x));

                if (intersection.length > 1) {
                    console.error(
                        `** IFRAME ERROR **\n` +
                        `Domains are not consistent with one another (you should explicitly set document.domain = ...)\n` +
                        `   %cparent          : '` + document.domain + `'\n   %ciframe (default): '` + iframeDefaultDomain + `'`,
                        'color: blue;',
                        'color: brown;'
                    );
                    console.log('💡 You may try to align these domains on:\n%c' + intersection.join('.'), 'color: blue;');
                } else {
                }

                return;
            }

            // Re-order children so as to remove space on top
            const parents = $(CSS_CLASSES_TO_KEEP, this.$iframe.document).parent();
            parents.children().not(CSS_CLASSES_TO_KEEP).remove();

            // Adjusting global height immediately (before even showing the page to the user)
            this.style.height = $('html', this.$iframe.document).height() + 'px';

            // -------------
            //   CSS magic
            // -------------

            // Listen for navigation change (beforeunload is not a good candidate...) within the iframe
            //   => hides the page until the CSS injection kicks in and CSS rules have been applied
            this._handleInternalNavigation(this.$iframe.window);

            // Listen for content resize and notifies
            this._autoResize($('html', this.$iframe.document)[0]);

            // Inject CSS into the iframe's page
            this._injectCSS(this.$iframe.document);

            // -------------
            //   Fallbacks
            // -------------

            // Sometimes, IE makes you mad...

         });
    }
}
