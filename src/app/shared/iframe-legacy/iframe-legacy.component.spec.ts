import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IframeLegacyComponent } from './iframe-legacy.component';

describe('IframeLegacyComponent', () => {
  let component: IframeLegacyComponent;
  let fixture: ComponentFixture<IframeLegacyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IframeLegacyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IframeLegacyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
