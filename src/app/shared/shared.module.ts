import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthService, ApiService, UploadService, ExportCSVService } from '../services/index';
import { NgxDatatableModule, DatatableComponent, TableColumn } from '@swimlane/ngx-datatable';
import { NguiDatetimePickerModule, NguiDatetime } from '@ngui/datetime-picker';
import { CookieService } from 'ngx-cookie-service';

import { HumanizePipe, TruncatePipe } from '../pipe/pipe';
import { RiskColorDirective } from '../directive/risk-color.directive';
import { ChartsModule } from 'ng2-charts';
import { MicroChartComponent } from './micro-chart/micro-chart.component';
import { IframeLegacyComponent } from './iframe-legacy/iframe-legacy.component';

NguiDatetime.months = [
    {fullName: 'Janvier', shortName: 'Janv'},
    {fullName: 'Février', shortName: 'Févr'},
    {fullName: 'Mars', shortName: 'Mars'},
    {fullName: 'Avril', shortName: 'Avril'},
    {fullName: 'Mai', shortName: 'Mai'},
    {fullName: 'Juin', shortName: 'Juin'},
    {fullName: 'Juillet', shortName: 'Juil'},
    {fullName: 'Août', shortName: 'Août'},
    {fullName: 'Septembre', shortName: 'Sept'},
    {fullName: 'Octobre', shortName: 'Oct'},
    {fullName: 'Novembre', shortName: 'Nov'},
    {fullName: 'Décembre', shortName: 'Dec'}
];
NguiDatetime.daysOfWeek = [
    {fullName: 'Dimanche', shortName: 'Di', weekend: true},
    {fullName: 'Lundi', shortName: 'Lu'},
    {fullName: 'Mardi', shortName: 'Ma'},
    {fullName: 'Mercredi', shortName: 'Me'},
    {fullName: 'Jeudi', shortName: 'Je'},
    {fullName: 'Vendredi', shortName: 'Ve'},
    {fullName: 'Samedi', shortName: 'Sa'}
];
NguiDatetime.firstDayOfWeek = 1;

@NgModule({
    imports: [
        CommonModule,
        NgxDatatableModule,
        NguiDatetimePickerModule,
        ChartsModule
    ],
    exports: [
        NguiDatetimePickerModule,
        NgxDatatableModule,
        ChartsModule,
        HumanizePipe,
        TruncatePipe,
        RiskColorDirective,
        MicroChartComponent,
        IframeLegacyComponent
    ],
    declarations: [
        HumanizePipe,
        RiskColorDirective,
        TruncatePipe,
        MicroChartComponent,
        IframeLegacyComponent
    ],
    providers: [
        AuthService,
        ApiService,
        UploadService,
        ExportCSVService,
        CookieService
    ]
})
export class SharedModule {
}
