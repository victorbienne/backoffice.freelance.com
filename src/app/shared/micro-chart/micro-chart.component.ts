import { Component, OnInit, Input, EventEmitter, ViewChild, OnChanges, SimpleChanges } from '@angular/core';
import { ChartsModule, BaseChartDirective } from 'ng2-charts';
import { RiskColorDirective } from './../../directive/risk-color.directive';

const LIGHT_GREY = 'rgba(200,200,200,0.25)';

@Component({
    selector: 'app-micro-chart',
    templateUrl: './micro-chart.component.html',
    styleUrls: ['./micro-chart.component.css']
})
export class MicroChartComponent implements OnChanges {
    // data: number[] | any[];   UNUSED
    datasets: any[]    = [{
        data           : [],
        backgroundColor: []
    }];
    labels: Array<any> = [];
    colors: Array<any> = [{}];  // /!\ Trick for'doughnut' type
    legend: boolean    = false;

    // tslint:disable-next-line:no-input-rename
    @Input('type') chartType: string = 'doughnut';
    @Input() value: number;
    @Input() label: any = '';

    @Input() options: any = {
        responsive         : false,  // come on, it is a micro-chart, how tinier could it be?!
        maintainAspectRatio: false,
        cutoutPercentage   : 40,
        events             : []  // disabled hover action (too small anyway)
    };
    @Input('color') color: any = '#00adef';
    @ViewChild(BaseChartDirective) public chart: BaseChartDirective;

    // tslint:disable-next-line:no-input-rename
    // @Input('hover') chartHover: EventEmitter<any>;
    // tslint:disable-next-line:no-input-rename
    // @Input('click') chartClick: EventEmitter<any>;

    constructor() {
    }

    getRiskColor(riskLevel) {
        return RiskColorDirective.getRiskLevelColor(riskLevel);
    }

    ngOnChanges(changes: SimpleChanges): void {
        const dataset = {
            data           : [],
            backgroundColor: []
        };
        const riskLevelColor = this.getRiskColor(this.color);

        dataset.data.push(...[this.value, 100 - this.value]);
        dataset.backgroundColor.push(riskLevelColor || this.color);
        dataset.backgroundColor.push(LIGHT_GREY);

        this.datasets[0] = dataset;
        this.labels.push(...[this.label, '']);

        this.chart.ngOnChanges(changes);   // See: https://github.com/valor-software/ng2-charts/issues/291
    }
}
