import {Component, Input, ContentChildren, ContentChild, ElementRef, EventEmitter, HostListener, AfterContentInit } from '@angular/core';
import { FabToggleComponent } from './fabToggle.component';
import { FabButtonComponent } from './fabButton.component';

@Component({
  selector: 'app-fab',
  template: `
    <nav
      class="fab-menu"
      [class.active]="active">
      <ng-content></ng-content>
    </nav>
  `
})
export class FabComponent implements AfterContentInit {
  _active;
  @Input() dir = 'right';
  element;
  @ContentChild(FabToggleComponent) toggle;
  @ContentChildren(FabButtonComponent) buttons;

  get active() {
    return this._active;
  }

  set active(val) {
    this.updateButtons(val);
    this._active = val;
  }

  constructor(element: ElementRef) {
    this.element = element.nativeElement;
  }

  ngAfterContentInit() {
    this.toggle.onClick.subscribe(() => {
      this.active = !this.active;
    });
  }

  getTranslate(idx) {
    if (this.dir === 'right') {
      return `translate3d(${ 60 * idx }px,0,0)`;
    } else if (this.dir === 'top') {
      return `translate3d(0,${ -60 * idx }px,0)`;
    } else {
      console.error(`Unsupported direction for Fab; ${this.dir}`);
    }
  }

  updateButtons(active) {
    let idx = 1;
    for (const btn of this.buttons.toArray()) {
      const style = btn.element.nativeElement.style;
      const childStyle = btn.element.nativeElement.children;
      style['transition-duration'] = active ? `${ 90 + (100 * idx) }ms` : '';
      style['transform'] = active ? this.getTranslate(idx) : '';
      childStyle[0].style.display = active ? 'block' : 'none';
      idx++;
    }
  }

  @HostListener('document:click', ['$event.target'])
  onDocumentClick(target) {
    if (this.active && !this.element.contains(target)) {
      this.active = false;
    }
  }

}
