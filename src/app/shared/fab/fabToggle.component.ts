import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-fab-toggle',
  template: `
    <a
      class="fab-toggle"
      (click)="onClick.emit($event)">
      <span
        [class]="'icon-' + icon">
      </span>
      <ng-content></ng-content>
    </a>
  `
})
export class FabToggleComponent {
  @Input() icon;
  @Output() onClick = new EventEmitter();
}
