import { Observable } from 'rxjs/Observable';
import { Component, ElementRef, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ROUTES } from '../sidebar/sidebar-routes.config';
import { MenuType } from '../sidebar/sidebar.metadata';
import { AuthService } from '../../services/index';
import { AppComponent } from '../../app.component';
import { environment } from 'environments/environment';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.sass']
})
export class NavbarComponent implements OnInit {
    private listTitles: any[];
    location: Location;
    notifications: Array<any> = [];

    constructor(
        private ref: ElementRef,
        private rootComp: AppComponent,
        private _authService: AuthService,
        location: Location) {
            this.location = location;
            this.refreshNotifications();
    }

    refreshNotifications() {
        if (!environment.production) {
            Observable.of([
                { title: 'Votre fiche de paie de Novembre 2017 est disponible', icon: 'pe-7s-cash', target: '/fichesdepaie' },
            ]).subscribe((data) => {
                this.notifications.push(...data);
            });
        }
    }

    ngOnInit() {
        this.listTitles = ROUTES.filter(listTitle => listTitle.menuType !== MenuType.BRAND);
    }

    getTitle() {
        let titlee = this.location.prepareExternalUrl(this.location.path());
        if (titlee.charAt(0) === '') {
            titlee = titlee.slice( 2 );
        }

        for (let item = 0; item < this.listTitles.length; item++) {
            if (this.listTitles[item].path === titlee) {
                return this.listTitles[item].title;
            }
        }
        return 'Dashboard';
    }

    setClass(newValue: boolean) {
        if (this.rootComp.sideToogle === true) {
            this.rootComp.sideToogle = false;
        } else {
            this.rootComp.sideToogle = newValue;
        }
    }

    setRightClass(newValue: boolean) {
        if (this.rootComp.setRightClass === true) {
            this.rootComp.setRightClass = false;
        }else {
            this.rootComp.setRightClass = newValue;
        }
    }

    logOut() {
        this._authService.logOut();
    }
}
