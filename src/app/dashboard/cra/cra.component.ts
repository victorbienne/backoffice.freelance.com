import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-cra',
    templateUrl: './cra.component.html',
    styleUrls: ['./cra.component.sass']
})
export class CraComponent {
    public url: string;

    constructor(private route: ActivatedRoute) {
        this.url = this.route.snapshot.data['remotePath'];
    }
}
