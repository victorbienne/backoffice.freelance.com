import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ApiService, AuthService, ExportCSVService, TableRowProcessor } from '../../services/index';
import { Order, User } from '../../models/index';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { DatatableComponent, TableColumn } from '@swimlane/ngx-datatable';

import * as moment from 'moment/moment';

moment.locale('fr-fr');
import 'rxjs/Rx';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {
    chart = {
        datasets: [{
            data: [20, 50],
            backgroundColor: ['rgb(51, 204, 255)', 'rgb(250, 160, 38)']
        }],
        labels: ['Portage salarial', 'Portage administratif'].map((label) => label.toUpperCase()),
        colors: [{}],  // Trick: https://github.com/valor-software/ng2-charts/issues/251
        options: {
            responsive: true,
            maintainAspectRatio: false,
            cutoutPercentage: 70
        }
    };

    currentUser: User;
    rows: any[];
    row: any = Order;
    limit = 8;
    loadingIndicator: Boolean = true;
    tableMessages = {emptyMessage: 'Pas de donnée disponible', totalMessage: 'enregistrement(s)'};
    contractData: any[];
    sumPrestation: any[];
    colorScheme = {domain: ['#45c0fd', '#ff2e2e', '#AAAAAA']};
    colorSchemeTree = {domain: ['#FFFFFF']};
    @ViewChild('ShownModal') public ShownModal: ModalDirective;
    @ViewChild('dataTable')  public dataTable: DatatableComponent;

    constructor(
        private elementRef: ElementRef,
        private _apiService: ApiService,
        private _authService: AuthService,
        private _exportCSVService: ExportCSVService) {
        this.currentUser = this._authService.getUser();
    }

    ngOnInit() {
        this.getLatestOrder();
        this.coutOrderProgress();
    }

    public exportAsCSV() {
        const processor: TableRowProcessor = (column: TableColumn, tableRow: any, reportRow: any): boolean => {
            const prop = column.prop;
            let skip = false;

            switch (prop) {
                case 'fullNameContractor':
                    reportRow[prop] =
                        tableRow.docDetails
                            .map((detail) => detail.fullNameContractor)
                            .filter((e) => e)
                            .join(' - ');
                    break;
                case 'ctNum':
                    reportRow[prop] = this.getStatus(tableRow[prop]);
                    break;
                case 'active':
                    reportRow[prop] = (tableRow[prop]) ? 'En cours' : 'Terminé';
                    break;
                default     :
                    skip = this._exportCSVService.defaultDataProcessor(column, tableRow, reportRow);
                    break;
            }
            return skip;
        };

        this._exportCSVService.exportAsCSV(
            this.dataTable,
            'Commandes',
            processor
        );
    }

    public getStatus(num): string {
        let status: string;
        switch (num) {
            case '401VCS'  :
            case '401VALOR':
            case '401VONE' :
            case '401ADM'  : status = 'Portage salarial';
            break;
            default        : status = 'Portage administratif';
        }
        return status;
    }

    public getLatestOrder() {
        this._apiService.get('orders', {running: true, structure: 'FCOM'}).then((res) => {
            this.rows = res;

            // Compute chart data
            this.chart.datasets[0].data.length = 0;  // reset array without losing Angular binding
            const weightedStatus = this.rows
                .map((entry) => this.getStatus(entry.ctNum).toUpperCase())  // Always use UPPERCASE to avoid mismatching
                .reduce((summary, status) => {
                    summary[status] = (summary[status] || 0) + 1;
                    return summary;
                }, {});  // {SALARIÉ: 2, PORTÉ: 1}
            this.chart.labels.forEach((label) => {
                this.chart.datasets[0].data.push(weightedStatus[label]);
            });

            setTimeout(() => {
                this.loadingIndicator = false;
            }, 1000);
        }).catch((error: Response) => {
            console.log(error);
        });
    }

    public getOrderDetail(pieceNumber: number) {
        this.ShownModal.show();
        this.row = this.rows.find(function (node) {
            return node.pieceNumber === pieceNumber;
        });
        for (const i of this.row.docDetails) {
            i.expireTime = moment(moment.unix(moment(i.deliveryDate).unix())).fromNow();
        }
    }

    public coutOrderProgress() {
        this._apiService.get('/orders/count/progress').then((res) => {
            this.sumPrestation = res.map(function (obj) {
                return {
                    name: 'Nombres',
                    value: obj.count
                }
            });
        });
    }

    public hideModal(): void {
        this.ShownModal.hide();
    }
}
