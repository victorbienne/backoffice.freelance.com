import { Component, OnInit, ViewChild, AfterViewInit, AfterContentInit, OnChanges, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IframeLegacyComponent } from './../../shared/iframe-legacy/iframe-legacy.component';

declare var jquery: any;
declare var $: any;

@Component({
    selector: 'app-events',
    templateUrl: './events.component.html',
    styleUrls: ['./events.component.sass']
})
export class EventsComponent implements OnInit, AfterContentInit {
    public url: string;
    @ViewChild('legacy') public legacy: IframeLegacyComponent;

    constructor(private route: ActivatedRoute) {
        this.url = this.route.snapshot.data['remotePath'];
    }

    ngOnInit() {
    }

    ngAfterContentInit(): void {
        this.removeTargetFromHyperlink(0);
    }

    public removeTargetFromHyperlink(delay: number = 0) {
        // TODO: replace this by an Observer
        if (this.legacy.$iframe.document) {
            setTimeout(() => {
                $('.pageTab a[target="_blank"]', this.legacy.$iframe.document).removeAttr('target');
                console.log('Target _blank removed from Events page');
            }, delay);
        } else {
            setTimeout(() => { this.removeTargetFromHyperlink(); }, Math.max(delay, 100));
        }
    }
}
