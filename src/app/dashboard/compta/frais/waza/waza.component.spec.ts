import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WazaComponent } from './waza.component';

describe('WazaComponent', () => {
  let component: WazaComponent;
  let fixture: ComponentFixture<WazaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WazaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WazaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
