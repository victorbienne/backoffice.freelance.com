import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-contacttransfere',
  templateUrl: './contacttransfere.component.html',
  styleUrls: ['./contacttransfere.component.sass']
})
export class ContacttransfereComponent implements OnInit {

    public url: string;

    constructor(private route: ActivatedRoute) {
        this.url = this.route.snapshot.data['remotePath'];
    }

  ngOnInit() {
  }

}
