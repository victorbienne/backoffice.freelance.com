import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContacttransfereComponent } from './contacttransfere.component';

describe('ContacttransfereComponent', () => {
  let component: ContacttransfereComponent;
  let fixture: ComponentFixture<ContacttransfereComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContacttransfereComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContacttransfereComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
