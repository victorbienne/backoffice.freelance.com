import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistotauxComponent } from './histotaux.component';

describe('HistotauxComponent', () => {
  let component: HistotauxComponent;
  let fixture: ComponentFixture<HistotauxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistotauxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistotauxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
