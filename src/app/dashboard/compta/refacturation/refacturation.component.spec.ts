import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RefacturationComponent } from './refacturation.component';

describe('RefacturationComponent', () => {
  let component: RefacturationComponent;
  let fixture: ComponentFixture<RefacturationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RefacturationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RefacturationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
