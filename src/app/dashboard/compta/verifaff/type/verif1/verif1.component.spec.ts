import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Verif1Component } from './verif1.component';

describe('Verif1Component', () => {
  let component: Verif1Component;
  let fixture: ComponentFixture<Verif1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Verif1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Verif1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
