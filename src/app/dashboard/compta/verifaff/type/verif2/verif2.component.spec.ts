import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Verif2Component } from './verif2.component';

describe('Verif2Component', () => {
  let component: Verif2Component;
  let fixture: ComponentFixture<Verif2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Verif2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Verif2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
