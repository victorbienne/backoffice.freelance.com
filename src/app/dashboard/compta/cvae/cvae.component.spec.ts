import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CvaeComponent } from './cvae.component';

describe('CvaeComponent', () => {
  let component: CvaeComponent;
  let fixture: ComponentFixture<CvaeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CvaeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CvaeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
