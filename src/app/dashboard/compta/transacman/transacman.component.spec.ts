import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransacmanComponent } from './transacman.component';

describe('TransacmanComponent', () => {
  let component: TransacmanComponent;
  let fixture: ComponentFixture<TransacmanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransacmanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransacmanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
