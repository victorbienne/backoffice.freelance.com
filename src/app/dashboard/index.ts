export * from './dashboard.component';
export * from './navbar/navbar.component';
export * from './sidebar/sidebar.component';
export * from './footer/footer.component';
export * from './home/home.component';
export * from './users/user-profile.component';
export * from './payslips/payslips.component';
export * from './cra/cra.component';
export * from './documents/documents.component';
export * from './missions/missions.component';
export * from './training/training.component'
export * from './events/events.component';
export * from './expenses/expenses.component';
export * from './purchaseorders/purchaseorders.component';
export * from './contact/contact.component';
export * from './community/community.component';
export * from './facturation/facturation.component';
export * from './outils/outils.component';
export * from './documents/contrat/contrat.component';
export * from './documents/cdi/cdi.component';
export * from './contact/new/new.component';
export * from './contact/suivi/suivi.component';
export * from './contact/chargercontacts/chargercontacts.component';
export * from './entreprises/entreprises.component';
export * from './entreprises/suivientreprises/suivientreprises.component';
export * from './facturation/referentiel/referentiel.component';
export * from './facturation/referentiel/search/search.component';
export * from './facturation/referentiel/new/new.component';
export * from './facturation/client/client.component';
export * from './contact/prescripteurs/prescripteurs.component';
export * from './contact/employes/employes.component';
export * from './facturation/bdc/bdc.component';
export * from './outils/event/index/type/one/one.component';
export * from './outils/event/index/type/two/two.component';
export * from './outils/tvs/tvs.component';
export * from './outils/frais/frais.component';
export * from './outils/anciensfrais/anciensfrais.component';
export * from './outils/cheque/cheque.component';
export * from './outils/missionsoutils/missionsoutils.component';
export * from './outils/parrainage/parrainage.component';
export * from './outils/formations/formations.component';
export * from './outils/interventions/interventions.component';
export * from './compta/verifaff/type/verif1/verif1.component';
export * from './compta/verifaff/type/verif2/verif2.component';
export * from './compta/transacman/transacman.component';
export * from './compta/relances/relances.component';
export * from './compta/contacttransfere/contacttransfere.component';
export * from './compta/contentieux/contentieux.component';
export * from './compta/histotaux/histotaux.component';
export * from './compta/frais/waza/waza.component';
export * from './compta/cvae/cvae.component';
export * from './compta/refacturation/refacturation.component';
export * from './compta/cotations/cotations.component';
export * from './communication/newsletters/newsletters.component';
export * from './communication/emailing/emailing.component';
export * from './communication/blog/blog.component';
export * from './communication/faq/faq.component';
export * from './stats/globales/globales.component';
export * from './stats/compterendu/region/zero/zero.component';
export * from './map/map.component';
export * from './stats/consultantsstats/consultantsstats.component';
export * from './stats/comparaison/comparaison.component';
export * from './stats/rrh/rrh.component';
export * from './stats/structures/structures.component';
export * from './requeteur/indexrequeteur/indexrequeteur.component';
