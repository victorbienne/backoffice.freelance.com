import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ModalModule, ButtonsModule, TooltipModule, AccordionModule, BsDropdownModule, PopoverModule, TabsModule } from 'ngx-bootstrap';
import { SharedModule } from '../shared/shared.module';

import {
  DashboardComponent,
  NavbarComponent,
  SidebarComponent,
  FooterComponent,
  HomeComponent,
  PayslipsComponent,
  CraComponent,
  DocumentsComponent,
  MissionsComponent,
  TrainingComponent,
  EventsComponent,
  ExpensesComponent,
  PurchaseordersComponent,
  ContactComponent,
  CommunityComponent
} from './index';
import { UserProfileComponent } from './users/user-profile.component';
import { FabComponent, FabToggleComponent, FabButtonComponent } from '../shared/fab/index';
import { SafePipe } from './../pipe/pipe';
import { FacturationComponent } from './facturation/facturation.component';
import { OutilsComponent } from './outils/outils.component';
import { ContratComponent } from './documents/contrat/contrat.component';
import { CdiComponent } from './documents/cdi/cdi.component';
import { NewComponent } from './contact/new/new.component';
import { SuiviComponent } from './contact/suivi/suivi.component';
import { ChargercontactsComponent } from './contact/chargercontacts/chargercontacts.component';
import { EntreprisesComponent } from './entreprises/entreprises.component';
import { SuivientreprisesComponent } from './entreprises/suivientreprises/suivientreprises.component';
import { ReferentielComponent } from './facturation/referentiel/referentiel.component';
import { SearchComponent } from './facturation/referentiel/search/search.component';
import { NewRefComponent } from './facturation/referentiel/new/new.component';
import { PrescripteursComponent } from './contact/prescripteurs/prescripteurs.component';
import { EmployesComponent } from './contact/employes/employes.component';
import { ClientComponent } from './facturation/client/client.component';
import { BdcComponent } from './facturation/bdc/bdc.component';
import { OneComponent } from './outils/event/index/type/one/one.component';
import { TwoComponent } from './outils/event/index/type/two/two.component';
import { TvsComponent } from './outils/tvs/tvs.component';
import { FraisComponent } from './outils/frais/frais.component';
import { AnciensfraisComponent } from './outils/anciensfrais/anciensfrais.component';
import { ChequeComponent } from './outils/cheque/cheque.component';
import { MissionsoutilsComponent } from './outils/missionsoutils/missionsoutils.component';
import { ParrainageComponent } from './outils/parrainage/parrainage.component';
import { FormationsComponent } from './outils/formations/formations.component';
import { InterventionsComponent } from './outils/interventions/interventions.component';
import { Verif2Component } from './compta/verifaff/type/verif2/verif2.component';
import { Verif1Component } from './compta/verifaff/type/verif1/verif1.component';
import { TransacmanComponent } from './compta/transacman/transacman.component';
import { AgmCoreModule } from '@agm/core';
import { RelancesComponent } from './compta/relances/relances.component';
import { ContacttransfereComponent } from './compta/contacttransfere/contacttransfere.component';
import { ContentieuxComponent } from './compta/contentieux/contentieux.component';
import { HistotauxComponent } from './compta/histotaux/histotaux.component';
import { WazaComponent } from './compta/frais/waza/waza.component';
import { CvaeComponent } from './compta/cvae/cvae.component';
import { RefacturationComponent } from './compta/refacturation/refacturation.component';
import { CotationsComponent } from './compta/cotations/cotations.component';
import { NewslettersComponent } from './communication/newsletters/newsletters.component';
import { EmailingComponent } from './communication/emailing/emailing.component';
import { BlogComponent } from './communication/blog/blog.component';
import { FaqComponent } from './communication/faq/faq.component';
import { GlobalesComponent } from './stats/globales/globales.component';
import { ZeroComponent } from './stats/compterendu/region/zero/zero.component';
import { MapComponent } from './map/map.component';
import { ConsultantsstatsComponent } from './stats/consultantsstats/consultantsstats.component';
import { ComparaisonComponent } from './stats/comparaison/comparaison.component';
import { RrhComponent } from './stats/rrh/rrh.component';
import { StructuresComponent } from './stats/structures/structures.component';
import { IndexrequeteurComponent } from './requeteur/indexrequeteur/indexrequeteur.component';

@NgModule({
  imports: [
    BrowserModule,
    RouterModule,
    NgxChartsModule,
    BrowserAnimationsModule,
    ModalModule.forRoot(),
    ButtonsModule.forRoot(),
    TooltipModule.forRoot(),
    AccordionModule.forRoot(),
    BsDropdownModule.forRoot(),
    PopoverModule.forRoot(),
    FormsModule,
    SharedModule,
    TabsModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyA11vsrnnu8n503jBwXbSU9rnrwP2WmeGg'
    })
  ],
  declarations: [
    // Components
    DashboardComponent,
    NavbarComponent,
    SidebarComponent,
    HomeComponent,
    FooterComponent,
    UserProfileComponent,
    FabComponent,
    FabToggleComponent,
    FabButtonComponent,
    PayslipsComponent,
    CraComponent,
    DocumentsComponent,
    MissionsComponent,
    TrainingComponent,
    EventsComponent,
    ExpensesComponent,
    PurchaseordersComponent,
    ContactComponent,
    CommunityComponent,
    NewRefComponent,

    // Pipes
    SafePipe,

    FacturationComponent,

    OutilsComponent,

    ContratComponent,

    CdiComponent,

    NewComponent,

    SuiviComponent,

    ChargercontactsComponent,

    EntreprisesComponent,

    SuivientreprisesComponent,

    ReferentielComponent,

    SearchComponent,

    PrescripteursComponent,

    EmployesComponent,

    ClientComponent,

    BdcComponent,

    OneComponent,

    TwoComponent,

    TvsComponent,

    FraisComponent,

    AnciensfraisComponent,

    ChequeComponent,

    MissionsoutilsComponent,

    ParrainageComponent,

    FormationsComponent,

    InterventionsComponent,

    Verif2Component,

    Verif1Component,

    TransacmanComponent,

    RelancesComponent,

    ContacttransfereComponent,

    ContentieuxComponent,

    HistotauxComponent,

    WazaComponent,

    CvaeComponent,

    RefacturationComponent,

    CotationsComponent,

    NewslettersComponent,

    EmailingComponent,

    BlogComponent,

    FaqComponent,

    GlobalesComponent,

    ZeroComponent,

    MapComponent,

    ConsultantsstatsComponent,

    ComparaisonComponent,

    RrhComponent,

    StructuresComponent,

    IndexrequeteurComponent
  ]
})

export class DashboardModule {
}
