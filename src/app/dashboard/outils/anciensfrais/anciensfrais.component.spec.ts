import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnciensfraisComponent } from './anciensfrais.component';

describe('AnciensfraisComponent', () => {
  let component: AnciensfraisComponent;
  let fixture: ComponentFixture<AnciensfraisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnciensfraisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnciensfraisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
