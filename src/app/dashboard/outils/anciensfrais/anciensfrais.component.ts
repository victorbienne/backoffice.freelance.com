import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-anciensfrais',
  templateUrl: './anciensfrais.component.html',
  styleUrls: ['./anciensfrais.component.sass']
})
export class AnciensfraisComponent implements OnInit {

    public url: string;

    constructor(private route: ActivatedRoute) {
        this.url = this.route.snapshot.data['remotePath'];
    }

  ngOnInit() {
  }

}
