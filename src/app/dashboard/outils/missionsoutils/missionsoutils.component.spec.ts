import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MissionsoutilsComponent } from './missionsoutils.component';

describe('MissionsoutilsComponent', () => {
  let component: MissionsoutilsComponent;
  let fixture: ComponentFixture<MissionsoutilsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MissionsoutilsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MissionsoutilsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
