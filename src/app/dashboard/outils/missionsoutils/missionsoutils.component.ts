import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-missionsoutils',
  templateUrl: './missionsoutils.component.html',
  styleUrls: ['./missionsoutils.component.sass']
})
export class MissionsoutilsComponent implements OnInit {

    public url: string;

    constructor(private route: ActivatedRoute) {
        this.url = this.route.snapshot.data['remotePath'];
    }

  ngOnInit() {
  }

}
