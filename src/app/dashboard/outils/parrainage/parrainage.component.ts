import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-parrainage',
  templateUrl: './parrainage.component.html',
  styleUrls: ['./parrainage.component.sass']
})
export class ParrainageComponent implements OnInit {

    public url: string;

    constructor(private route: ActivatedRoute) {
        this.url = this.route.snapshot.data['remotePath'];
    }

  ngOnInit() {
  }

}
