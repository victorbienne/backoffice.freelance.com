import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-facturation',
  templateUrl: './facturation.component.html',
  styleUrls: ['./facturation.component.sass']
})
export class FacturationComponent implements OnInit {

    public url: string;

    constructor(private route: ActivatedRoute) {
        this.url = this.route.snapshot.data['remotePath'];
    }

    ngOnInit() {
    }

}
