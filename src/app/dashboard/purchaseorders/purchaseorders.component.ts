import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-purchaseorders',
    templateUrl: './purchaseorders.component.html',
    styleUrls: ['./purchaseorders.component.sass']
})
export class PurchaseordersComponent implements OnInit {
    public url: string;

    constructor(private route: ActivatedRoute) {
        this.url = this.route.snapshot.data['remotePath'];
    }

    ngOnInit() {
    }

}
