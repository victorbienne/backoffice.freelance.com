import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultantsstatsComponent } from './consultantsstats.component';

describe('ConsultantsstatsComponent', () => {
  let component: ConsultantsstatsComponent;
  let fixture: ComponentFixture<ConsultantsstatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultantsstatsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultantsstatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
