import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-consultantsstats',
  templateUrl: './consultantsstats.component.html',
  styleUrls: ['./consultantsstats.component.sass']
})
export class ConsultantsstatsComponent implements OnInit {

    public url: string;

    constructor(private route: ActivatedRoute) {
        this.url = this.route.snapshot.data['remotePath'];
    }

  ngOnInit() {
  }

}
