import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-globales',
  templateUrl: './globales.component.html',
  styleUrls: ['./globales.component.sass']
})
export class GlobalesComponent implements OnInit {

    public url: string;

    constructor(private route: ActivatedRoute) {
        this.url = this.route.snapshot.data['remotePath'];
    }

  ngOnInit() {
  }

}
