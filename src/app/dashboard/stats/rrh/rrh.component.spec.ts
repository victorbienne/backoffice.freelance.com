import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RrhComponent } from './rrh.component';

describe('RrhComponent', () => {
  let component: RrhComponent;
  let fixture: ComponentFixture<RrhComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RrhComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RrhComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
