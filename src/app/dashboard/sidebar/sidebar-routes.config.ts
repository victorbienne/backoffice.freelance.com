import { MenuType, RouteInfo } from './sidebar.metadata';

export const ROUTES: RouteInfo[] = [
    // { path: '',                 title: 'Dashboard',        menuType: MenuType.LEFT, icon: 'pe-7s-graph' },
    { path: '/',                title: `Home`,         menuType: MenuType.LEFT, icon: 'pe-7s-portfolio'},

    { path: '/contact',                title: 'Contact',         menuType: MenuType.LEFT, icon: 'pe-7s-users',  btnGroup: [
        { title: 'Consultants',   btns: [
            { print: '<i class="pe-7s-plus pe-size-2" style="font-size: 2.5em;" ></i>',    route: '/contact/new', popover: `Création d'une fiche contact` },
            { print: '<i class="pe-7s-search pe-size-2" style="font-size: 2.5em;"></i>',    route: '/contact', popover: `Rechercher un contact` },
            { print: '<i class="pe-7s-menu pe-size-2" style="font-size: 2.5em;"></i>',    route: '/contact/suivi', popover: `Suivi consultant` },
            { print: 'Générer des Prospects',    route: '/contact/chargercontacts', popover: `Générer des Prospects` }
        ]},
        { title: 'Entreprises',   btns: [
            { print: 'Prospects/Clients',    route: '/entreprises', popover: '' },
            { print: 'Clients GESCO',    route: '/entreprises/suivientreprises', popover: '' },
        ]},
        { title: 'Prescripteurs',   btns: [
            { print: 'Prescripteurs',    route: '/prescripteurs', popover: '' },
        ]}
    ]},

    { path: '/facturation',                title: `Facturation`,         menuType: MenuType.LEFT, icon: 'pe-7s-cash', btnGroup: [
        { title: 'Référentiel Clients facturation', btns: [
            { print: 'Liste des clients',   route: '/referentiel',  popover: ''},
            { print: 'Rechercher un client',   route: '/referentiel/search',  popover: ''},
            { print: 'Création d\'un client',   route: '/referentiel/new',  popover: ''},
        ]},
        { title: 'Facturation en attente',  btns: [
            { print: 'Facturation en attente',  route: '/facturation',  popover: '' }
        ]},
        { title: 'Gestion des BDC',  btns: [
            { print: 'Gestion des BDC',  route: '/bdc',  popover: '' }
        ]},
        { title: 'Gestion des BDC',  btns: [
            { print: 'Gestion des clients',  route: '/client',  popover: '' }
        ]},
        { title: 'Gestion des CRA',  btns: [
            { print: 'Gestion des CRA',  route: '/cra',  popover: '' }
        ]},
    ]},

    { path: '/dashboard',                title: `Outils`,         menuType: MenuType.LEFT, icon: 'pe-7s-tools', btnGroup: [
        { title: 'Gestion des Evénements',  btns: [
            { print: 'Evénements',  route: '/event/index/type/1', popover: '' },
            { print: 'Reunions d\'Info',  route: '/event/index/type/2', popover: '' },
        ]},
        { title: 'Tableau de bord',  btns: [
            { print: 'Tableau de bord',  route: '/dashboard', popover: '' },
        ]},
        { title: 'Gestion de la TVS',  btns: [
            { print: 'Gestion de la TVS',  route: '/tvs', popover: '' },
        ]},
        { title: 'Gestion des Notes de Frais',  btns: [
            { print: 'Gestion des Notes de Frais',  route: '/frais', popover: '' },
        ]},
        { title: 'Gestion anciennes Notes de Frais',  btns: [
            { print: 'Gestion anciennes Notes de Frais',  route: '/anciensfrais', popover: '' },
        ]},
        { title: 'Gestion des Chèques Clients',  btns: [
            { print: 'Gestion des Chèques Clients',  route: '/cheque', popover: '' },
        ]},
        { title: 'Gestion des Offres de Missions',  btns: [
            { print: 'Gestion des Offres de Missions',  route: '/missions', popover: '' },
        ]},
        { title: 'Gestion des Parrainages',  btns: [
            { print: 'Gestion des Parrainages',  route: '/parrainage', popover: '' },
        ]},
        { title: 'Gestion de la formation',  btns: [
            { print: 'Gestion de la formation',  route: '/formations', popover: '' },
        ]},
        { title: 'Demandes d\'intervention',  btns: [
            { print: 'Demandes d\'intervention',  route: '/interventions', popover: '' },
        ]},
    ]},
    { path: '/contrat',                title: `Documents`,         menuType: MenuType.LEFT, icon: 'pe-7s-folder', btnGroup: [
        { title: 'Documents', btns: [
            { print: 'Contrats de travail', route: '/contrat', popover: '' },
            { print: 'GESTION DES CDI', route: '/contact/lettre', popover: '' },
        ] }
    ] },

    { path: '/compta',                title: `Compta`,         menuType: MenuType.LEFT, icon: 'pe-7s-calculator', btnGroup: [
        { title: 'Affectation', btns: [
            { print: 'Factures',    route: '/compta/verifaff/type/1',   popover: '' },
            { print: 'Masses Salariales',    route: '/compta/verifaff/type/2',   popover: '' },
        ] },
        { title: 'Transactions Manuelles', btns: [
            { print: 'Transactions Manuelles',    route: '/transacman',   popover: '' },
        ] },
        { title: 'Relances', btns: [
            { print: 'Relances',    route: '/relances',   popover: '' },
        ] },
        { title: 'Transferts Consultants', btns: [
            { print: 'Transferts Consultants',    route: '/compta/contacttransfere',   popover: '' },
        ] },
        { title: 'Dossiers Contentieux', btns: [
            { print: 'Dossiers Contentieux',    route: '/contentieux',   popover: '' },
        ] },
        { title: 'Histo taux WAZA', btns: [
            { print: 'Histo taux WAZA',    route: '/compta/histotaux',   popover: '' },
        ] },
        { title: 'Gestion Frais', btns: [
            { print: 'Gestion Frais',    route: '/frais/waza',   popover: '' },
        ] },
        { title: 'CVAE', btns: [
            { print: 'CVAE',    route: '/compta/cvae',   popover: '' },
        ] },
        { title: 'Refacturation', btns: [
            { print: 'Refacturation',    route: '/compta/refacturation',   popover: '' },
        ] },
        { title: 'Cotations Coface', btns: [
            { print: 'Cotations Coface',    route: '/compta/cotations',   popover: '' },
        ] },
    ] },

    { path: '/communication',                title: `Communication`,         menuType: MenuType.LEFT, icon: 'pe-7s-comment', btnGroup: [
        { title: 'Newsletter', btns: [
            { print: 'Newsletter', route: '/newsletters', popover: '' }
        ] },
        { title: 'Emailing', btns: [
            { print: 'Emailing', route: '/mailing', popover: '' }
        ] },
        { title: 'Blog', btns: [
            { print: 'Blog', route: '/blog', popover: '' }
        ] },
        { title: 'Questions fréquentes', btns: [
            { print: 'Questions fréquentes', route: '/faq', popover: '' }
        ] },
    ] },

    // { path: '/missions',        title: `Missions`,         menuType: MenuType.LEFT, icon: 'pe-7s-portfolio' },
    // { path: '/contact',         title: `Contact`,          menuType: MenuType.LEFT, icon: '', class: 'hidden' },
    // { path: '/training',        title: `Formations`,       menuType: MenuType.LEFT, icon: 'pe-7s-display1' },
    // { path: '/events',          title: `Événements`,       menuType: MenuType.LEFT, icon: 'pe-7s-way' },
    // { path: '/payslips',        title: `Fiches de paie`,   menuType: MenuType.LEFT, icon: 'pe-7s-cash' },
    // { path: '/cra',             title: `C.R.A.`,           menuType: MenuType.LEFT, icon: 'pe-7s-date' },
    // { path: '/expenses',        title: `Notes de frais`,   menuType: MenuType.LEFT, icon: 'pe-7s-credit' },
    // { path: '/purchaseorders',  title: `Bons de commande`, menuType: MenuType.LEFT, icon: 'pe-7s-news-paper' },
    // { path: '/documents',       title: `Documents`,        menuType: MenuType.LEFT, icon: 'pe-7s-folder' },
    // { path: '/community',       title: `Community`,        menuType: MenuType.LEFT, icon: 'pe-7s-users', class: 'community badge-new' },
];
