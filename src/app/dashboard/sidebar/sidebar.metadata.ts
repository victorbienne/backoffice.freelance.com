export enum MenuType {
    BRAND,
    LEFT,
    RIGHT
}

export interface RouteInfo {
    path: string;
    title: string;
    menuType: MenuType;
    icon: string;
    btnGroup?: BtnGroup[];
    class?: string;
}

export interface BtnGroup {
    title: string;
    btns: Btn[];
}

export interface Btn {
    print: string;
    route: string;
    popover?: string;
}
