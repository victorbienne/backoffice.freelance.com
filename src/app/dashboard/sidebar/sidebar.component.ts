import { Component, NgZone, HostBinding, OnInit, ViewChild, ElementRef, Renderer, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { ROUTES } from './sidebar-routes.config';
import { MenuType } from './sidebar.metadata';
import { AuthService } from '../../services/index';
import { User } from '../../models/user';
import { environment } from '../../../environments/environment';
import { BsModalService, ModalDirective } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.sass']
})
export class SidebarComponent implements OnInit {

  config = {
    animated: true,
    keyboard: true,
    backdrop: true,
    ignoreBackdropClick: false
  };

  public modalRef: BsModalRef;
  menuItems: any[];
  currentUser: User;
  isCollapsed = true;
  public filePath = environment.api.filePath;
  public globalModal: string;
  public datasModal: string[];
  public img = {
    paris: 'assets/img/city/paris.png',
    lille: 'assets/img/city/lille.png',
    lyon: 'assets/img/city/lyon.png',
    bordeaux: 'assets/img/city/bordeaux.png',
    marseille: 'assets/img/city/marseille.png',
    alsacelorraine: 'assets/img/city/alsace-lorraine.png',
    nantes: 'assets/img/city/nantes.png',
    rennes: 'assets/img/city/rennes.png',
    occitanie: 'assets/img/city/occitanie.png',
    nice: 'assets/img/city/nice.png',
    grenoble: 'assets/img/city/grenoble.png',
    clermontferrand: 'assets/img/city/clermont-ferrand.png',
    saintnazaire: 'assets/img/city/saint-nazaire.png',
    rochesuryon: 'assets/img/city/roche-sur-yon.png',
    normandie: 'assets/img/city/normandie.png',
    lorraine: 'assets/img/city/lorraine.png',
  }
  public mapChoice: number = 1;

  @ViewChild('profileImage') profileImage: ElementRef;
  constructor(
    private _authService: AuthService,
    private router: Router,
    private renderer: Renderer,
    private modalService: BsModalService,
  ) {
    this.currentUser = this._authService.getUser();
    this.globalModal = '';
  }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem.menuType !== MenuType.BRAND);
  }
  public get menuIcon(): string {
    return this.isCollapsed ? '☰' : '✖';
  }
  public getMenuItemClasses(menuItem: any) {
    return {
      'pull-xs-right': this.isCollapsed && menuItem.menuType === MenuType.RIGHT
    };
  }

  public openModal(template: TemplateRef<any>, index: string): void {
    this.globalModal = this.menuItems[index].title;
    this.datasModal = this.menuItems[index].btnGroup;
    console.log(this.datasModal);
    this.modalRef = this.modalService.show(template);
  }

  public openMap(template: TemplateRef<any>): void {
    this.modalRef = this.modalService.show(
      template,
      Object.assign({}, this.config, { class: 'modal-lg' })
    );
  }

  public checkChildren(item): boolean {
    console.log(typeof item.children);
    if (item.children == undefined) {
      return true;
    } else {
      return false;
    }
  }

  public map(city) {
    if (this.mapChoice === 1) {
      this.router.navigateByUrl('map/' + city.toLowerCase());
    } else {
      this.router.navigateByUrl('compterendu/' + city.toLowerCase());
    }
  }


  logOut() {
    this._authService.logOut();
  }

}
