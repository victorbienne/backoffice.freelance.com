import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChargercontactsComponent } from './chargercontacts.component';

describe('ChargercontactsComponent', () => {
  let component: ChargercontactsComponent;
  let fixture: ComponentFixture<ChargercontactsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChargercontactsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChargercontactsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
