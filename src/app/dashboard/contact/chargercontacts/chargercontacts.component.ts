import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-chargercontacts',
  templateUrl: './chargercontacts.component.html',
  styleUrls: ['./chargercontacts.component.sass']
})
export class ChargercontactsComponent implements OnInit {

    public url: string;

    constructor(private route: ActivatedRoute) {
        this.url = this.route.snapshot.data['remotePath'];
    }

  ngOnInit() {
  }

}
