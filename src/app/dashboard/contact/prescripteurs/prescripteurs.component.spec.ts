import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrescripteursComponent } from './prescripteurs.component';

describe('PrescripteursComponent', () => {
  let component: PrescripteursComponent;
  let fixture: ComponentFixture<PrescripteursComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrescripteursComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrescripteursComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
