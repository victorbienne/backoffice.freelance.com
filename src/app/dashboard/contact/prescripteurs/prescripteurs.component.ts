import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-prescripteurs',
  templateUrl: './prescripteurs.component.html',
  styleUrls: ['./prescripteurs.component.sass']
})
export class PrescripteursComponent implements OnInit {

    public url: string;

    constructor(private route: ActivatedRoute) {
        this.url = this.route.snapshot.data['remotePath'];
    }

  ngOnInit() {
  }

}
