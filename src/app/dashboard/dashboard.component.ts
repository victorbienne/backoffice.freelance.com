import {Component, OnInit, ViewChild, ViewEncapsulation, NgZone} from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import {ModalDirective} from 'ngx-bootstrap/modal';
import {SinFormData, User} from '../models/index';
import {ApiService, AuthService} from '../services/index';

import { FabComponent } from './../shared/fab/fab.component';

declare const swal: any;
declare const $: any;
declare const moment: any;


declare const google: any;
@Component({
    selector: 'app-dashboard',
    templateUrl: 'dashboard.component.html',
    styleUrls: ['dashboard.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class DashboardComponent implements OnInit {
    @ViewChild('ShownModal') public ShownModal: ModalDirective;
    @ViewChild('fab')        public fab: FabComponent;

    currentUser: User;
    formData: any = SinFormData;
    publicList: any;
    public minStartDate = new Date();
    public locations = [
        {value: 'ONSITE', display: 'Sur site'},
        {value: 'REMOTE', display: 'À distance'}
    ];
    public rates = [
        {value: true, display: 'Forfait'},
        {value: false, display: 'Régie'}
    ];

    constructor(
        private router: Router,
        private _apiService: ApiService,
        private _authService: AuthService,
        private _zone: NgZone) {
        moment.locale('fr-fr');
        this.currentUser = this._authService.getUser();

        // override the route reuse strategy
        this.router.routeReuseStrategy.shouldReuseRoute = () => false;

        this.router.events.subscribe((evt) => {
            if (evt instanceof NavigationEnd) {
                this.router.navigated = false;  // trick the Router: last link wasn't previously loaded
                window.scrollTo(0, 0);          // scroll back to top
            }
        });
    }

    ngOnInit() {
        this.formData = {
            location: this.locations[0].value,
            rate: this.rates[0].value,
        }
    }

    public redirectTo(endPoint): void {
        this.router.navigateByUrl(endPoint);
    }

    public urlChecker(words: string[]): boolean {
        for (const word of words) {
            if (this.router.url.indexOf(word) > -1) {
                return true;
            }
        }
        return false;
    }
}
