import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-indexrequeteur',
  templateUrl: './indexrequeteur.component.html',
  styleUrls: ['./indexrequeteur.component.sass']
})
export class IndexrequeteurComponent implements OnInit {

    public url: string;

    constructor(private route: ActivatedRoute) {
        this.url = this.route.snapshot.data['remotePath'];
    }

  ngOnInit() {
  }

}
