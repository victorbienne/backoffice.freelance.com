import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexrequeteurComponent } from './indexrequeteur.component';

describe('IndexrequeteurComponent', () => {
  let component: IndexrequeteurComponent;
  let fixture: ComponentFixture<IndexrequeteurComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndexrequeteurComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndexrequeteurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
