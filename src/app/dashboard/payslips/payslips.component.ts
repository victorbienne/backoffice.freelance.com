import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-payslips',
    templateUrl: './payslips.component.html',
    styleUrls: ['./payslips.component.sass']
})
export class PayslipsComponent implements OnInit {
    public url: string;

    constructor(private route: ActivatedRoute) {
        this.url = this.route.snapshot.data['remotePath'];
    }

    ngOnInit() {
    }

}
