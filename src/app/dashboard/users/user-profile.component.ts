import {Component, OnInit, Input, EventEmitter, ViewContainerRef, ViewChild, ElementRef, Renderer} from '@angular/core';
import {User} from '../../models/user';
import {AuthService, ApiService} from '../../services/index';
import {ToastsManager} from 'ng2-toastr';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.sass'],
})
export class UserProfileComponent implements OnInit {
  @Input() currentUser: User;
  public establishments: any = [];
  public submitted = false;
  private apiUrl = environment.api.url;
  public filePath = environment.api.filePath;
  maxSize = 5000000;
  invalidFileSizeMessage = '{0}: Invalid file size, ';
  invalidFileSizeMessageDetail = 'Maximum upload size is {0}.';
  onClear: EventEmitter<any> = new EventEmitter();
  public files: File[];
  public progress = 0;
  @ViewChild('fileInput') fileInput: ElementRef;
  @ViewChild('profileImage') profileImage: ElementRef;

  constructor(private _authService: AuthService,
              private _apiService: ApiService,
              private toastr: ToastsManager,
              vcr: ViewContainerRef,
              private renderer: Renderer) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    this.currentUser = this._authService.getUser();
    this._apiService.get('establishments').then((res) => {
      this.establishments = res;
    });
  }

  triggerFile() {
    this.fileInput.nativeElement.click();
  }

  onFileSelect(event) {
    this.clear();
    const files = event.dataTransfer ? event.dataTransfer.files : event.target.files;
    for (let i = 0; i < files.length; i++) {
      const file = files[i];
      if (this.validate(file)) {
        if (this.isImage(file)) {
          const urlCreator = window.URL;
          const url = urlCreator.createObjectURL(files[i]);
          this.renderer.setElementProperty(this.profileImage.nativeElement, 'src', url);
          this.files.push(files[i]);
          this.onUpload();
        }
      } else if (!this.isImage(file)) {
        this.toastr.error('Seules les images sont autorisées');
      }
    }
  }

  // check if the image is actually an image by checking the mime type
  isImage(file: File): boolean {
    if (!file.type.match('image/*')) {
      this.toastr.error('Seules les images sont autorisées');
      return false;
    }
    return true;
  }

  // check if the form has files ready to be uploaded
  hasFiles(): boolean {
    return this.files && this.files.length > 0;
  }

  // clears the form
  clear() {
    this.files = [];
    this.onClear.emit();
  }

  // remove the image from the preview
  remove(index: number) {
    this.files.splice(index, 1);
    this.fileInput.nativeElement.value = '';
  }

  // check the image file size
  validate(file: File): boolean {
    if (this.maxSize && file.size > this.maxSize) {
      this.toastr.error(this.invalidFileSizeMessageDetail.replace('{0}', this.formatSize(this.maxSize)),
        this.invalidFileSizeMessage.replace('{0}', file.name));
      return false;
    }
    return true;
  }

  // format the size to display it in toastr in case the user uploaded a file bigger than 5MB
  formatSize(bytes) {
    if (bytes === 0) {
      return '0 B';
    }
    const k = 1000,
      dm = 3,
      sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
      i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
  }

  onSubmit({value, valid}: { value: User, valid: boolean }) {
    this.submitted = true;
    this._apiService.put('users/update_profile', value).then((res) => {
      if (res.obj.ok === 1) {
        this.toastr.success('Votre profil a été mis à jour avec succès!', null,
          {toastLife: 6000});
      }
    });

  }

  onUpload() {
    this.submitted = true;
    const xhr = new XMLHttpRequest();
    const formData = new FormData();
    for (let i = 0; i < this.files.length; i++) {
      formData.append('profile_avatar', this.files[i], this.files[i].name);
    }
    xhr.upload.addEventListener('progress', (event: ProgressEvent) => {
      if (event.lengthComputable) {
        this.progress = Math.round((event.loaded * 100) / event.total);
      }
    }, false);
    xhr.onreadystatechange = () => {
      if (xhr.readyState === 4) {
        this.progress = 0;
        if (xhr.status === 201) {
          this.toastr.success('Votre image de profil a été mis à jour avec succès', null, {toastLife: 6000});
        } else if (xhr.status !== 201) {
          this.toastr.error('Erreur! Format image non pris en charge');
        }
        this.clear();
        this.submitted = false;
      }
    };

    xhr.open('POST', this.apiUrl + '/1/users/avatar', true);
    xhr.withCredentials = false;
    xhr.setRequestHeader('access-token', localStorage.getItem('authToken'));
    xhr.send(formData);
  }

}
