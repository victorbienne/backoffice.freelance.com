import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuivientreprisesComponent } from './suivientreprises.component';

describe('SuivientreprisesComponent', () => {
  let component: SuivientreprisesComponent;
  let fixture: ComponentFixture<SuivientreprisesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuivientreprisesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuivientreprisesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
