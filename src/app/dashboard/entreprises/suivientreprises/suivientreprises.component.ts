import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-suivientreprises',
  templateUrl: './suivientreprises.component.html',
  styleUrls: ['./suivientreprises.component.sass']
})
export class SuivientreprisesComponent implements OnInit {

    public url: string;

    constructor(private route: ActivatedRoute) {
        this.url = this.route.snapshot.data['remotePath'];
    }

  ngOnInit() {
  }

}
