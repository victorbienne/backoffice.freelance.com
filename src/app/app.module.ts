import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, LOCALE_ID } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { DashboardModule } from './dashboard/dashboard.module';
import { AuthComponent } from './auth/auth.component';
import { ForgotPasswordComponent } from './password/forgotPassword.component';
import { ResetPasswordComponent } from './password/resetPassword.component';
import { AppRoutingModule } from './app.routing';
// canActive Guards providers
import { AuthGuardGuard } from './auth-guard.guard';
import { ToastModule } from 'ng2-toastr/ng2-toastr';
import { EqualValidatorDirective } from './directive/equal-validator.directive';

@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,
    EqualValidatorDirective,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    DashboardModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ToastModule.forRoot(),

  ],
  providers: [
    AuthGuardGuard,
    { provide: LOCALE_ID, useValue: 'fr-FR' },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
