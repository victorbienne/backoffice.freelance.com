import {Component, OnInit, OnDestroy} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../services/index';
declare const $: any;
@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.sass']
})
export class AuthComponent implements OnInit, OnDestroy {
  model: any = {};
  loading = false;
  returnUrl: string;
  private sub: any;

  constructor(
    private _authService: AuthService,
    private route: ActivatedRoute,
    private router: Router) {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '';
  }

  ngOnInit() {
    this.model.userMail = '';
    this.sub = this.route
      .queryParams
      .subscribe(params => {
        if (params.msg === 'expToken') {
          $.notify({
            icon: '',
            message: 'Votre session a expiré! <br> Nous vous invitons à vous reconnecter'
          }, {
            type: 'success',
            delay: 6000,
            placement: {
              from: 'top',
              align: 'center',
            },
            template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0} text-center"' +
            ' role="alert">' +
            '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
            '<span data-notify="message">{2}</span>' +
            '</div>'
          });
        }else if (localStorage.getItem('logOut') === 'true') {
          $.notify({
            icon: '',
            message: 'Vous avez été déconnecté(e).! <br> Pour une sécurité maximale, fermez toutes les' +
            ' fenêtres de votre navigateur'
          }, {
            type: 'success',
            delay: 6000,
            placement: {
              from: 'top',
              align: 'center',
            },
            template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0} text-center"' +
            ' role="alert">' +
            '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
            '<span data-notify="message">{2}</span>' +
            '</div>'
          });
        }
        localStorage.clear();
      });
  }

  auth() {
    this.loading = true;
    this._authService.login(this.model).subscribe((result) => {
      if (!result) {
        this.router.navigate(['users', 'login']);
        this.loading = false;
        return false;
      }
      this.router.navigate([this.returnUrl]);
    }, error =>  {
      this.loading = false;
      $.notify({
        icon: '',
        message: error
      }, {
        type: 'danger',
        delay: 8000,
        placement: {
          from: 'top',
          align: 'center',
        },
        template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0} text-center"' +
        ' role="alert">' +
        '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
        '<span data-notify="message">{2}</span>' +
        '</div>'
      });
    });
  }

  displayPasswordForgot() {
    this.router.navigate(['users', 'forgot-password']);
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
