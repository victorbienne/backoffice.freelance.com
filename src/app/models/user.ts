export class User {
  _id: string;
  username: string;
  email: string;
  firstName: string;
  lastName: string;
  jobTitle: string;
  profile_avatar: string;
  address: any;
  activity: string;
}
