export class SinFormData {
  brand_id?: number;
  ticket_form_id?: number;
  subject?: string;
  priority?: string;
  tags?: any;
  type?: string;
  requester?: any;
  firstName?: string;
  lastName?: string;
  email?: string;
  tel?: string;
  title?: string;
  skills?: any;
  startDate?: string;
  endDate?: string;
  fixedPrice?: string;
  place?: string;
  comment?: string;
}
