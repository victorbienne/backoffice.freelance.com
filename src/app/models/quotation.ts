export class Quotation {
  id: string;
  pieceNumber: string;
  idContractor: number;
  fullNameContractor: string;
  structure: string;
  siretNumber: number;
  operationnel: string;
  totalHT: number;
  totalTTC: number;
  pieceDate: Date;
  mEndDate: Date;
  mStartDate: Date;
  docDetails?: object;
  establishmentName: string;
  establishmentAddress?: object;
}
