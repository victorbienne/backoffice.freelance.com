export class Docline {
  id: string;
  pieceNumber: string;
  idContractor: number;
  fullNameContractor: string;
  structure: string;
  siretNumber: number;
  totalHT: number;
  totalTTC: number;
  pieceDate: Date;
  docDetails: object;
  establishmentName: string;
}
