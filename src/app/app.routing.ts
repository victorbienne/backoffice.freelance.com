import {NgModule} from '@angular/core';
import {Routes, RouterModule, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {AuthComponent} from './auth/auth.component';
import {ForgotPasswordComponent} from './password/forgotPassword.component';
import {ResetPasswordComponent} from './password/resetPassword.component';
import {DashRoutes} from './dashboard/dashboard.routes';

export const routes: Routes = [
  ...DashRoutes,
  {path: 'users/login', component: AuthComponent},
  {path: 'users/forgot-password', component: ForgotPasswordComponent},
  {path: 'users/reset-password', component: ResetPasswordComponent},
  {path: '**', redirectTo: ''}
];

export function AuthGuardFactory(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
  return true;
}

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [
    {
      provide: 'AuthGuard',
      useValue: AuthGuardFactory
    }
  ]
})
export class AppRoutingModule {
}

