import {Injectable, Injector} from '@angular/core';
import 'rxjs/add/operator/toPromise';
import {Router} from '@angular/router';
import {
  Headers,
  Http,
  Request,
  RequestMethod,
  RequestOptions,
  Response,
  URLSearchParams,
  ResponseContentType
} from '@angular/http';
import {AuthService} from './auth.service';
import {environment} from '../../environments/environment';

export interface ApiOptions {
  enableToBlob?: boolean,
}

@Injectable()
export class ApiService {
  private apiUrl = environment.api.url + '/1/';
  private _authService: AuthService;

  public constructor(private http: Http,
                     private router: Router, private injector: Injector) {
  }

  /**
   *
   * @param method
   * @param action
   * @param params
   * @param body
   * @param headers
   * @param options
   * @returns {Promise<TResult|TResult2|TResult1>}
   */
  public queries(method: string, action: string, params: Object = {}, body: Object = {}, headers: Object = {},
                 options: ApiOptions = {}) {
    method = method.substr(0, 1).toUpperCase() + method.substr(1).toLowerCase();
    if (action[0] === '/') {
      action = action.substr(1);
    }

    if (this._authService === undefined) {
      this._authService = this.injector.get(AuthService);
    }
    const dataHeaders = new Headers();
    for (const key in headers) {
      dataHeaders.append(key, headers[key]);
    }

    if (this._authService.isLogged() === true) {
      dataHeaders.append('access-token', localStorage.getItem('authToken'));
    }

    const dataSearch = new URLSearchParams();
    for (const key in params) {
      dataSearch.append(key, params[key]);
    }

    if (options.enableToBlob === undefined) {
      options.enableToBlob = false;
    }
    let responseType = ResponseContentType.Json;
    if (options.enableToBlob === true) {
      responseType = ResponseContentType.Blob;
    }
    const requestOptions = new RequestOptions({
      method: RequestMethod[method],
      url: this.apiUrl + action,
      headers: dataHeaders,
      search: dataSearch,
      body: body,
      responseType: responseType
    });

    return this.http.request(new Request(requestOptions)).toPromise().then((res: Response) => {
      if (options.enableToBlob === true) {
        return new Blob([res.blob()], {type: 'application/pdf'});
      } else {
        return res.json();
      }
    }).catch((error) => {
      return this.handleError(error);
    });
  }

  public get(action: string, params: Object = {}, headers: Object = {}, options: ApiOptions = {}) {
    return this.queries('GET', action, params, null, headers, options);
  }

  public post(action: string, body: Object = {}, options: ApiOptions = {}) {
    return this.queries('POST', action, null, body, options);
  }

  public put(action: string, body: Object = {}, options: ApiOptions = {}) {
    return this.queries('PUT', action, null, body, options);
  }

  public patch(action: string, body: Object = {}, options: ApiOptions = {}) {
    return this.queries('PATCH', action, null, body, options);
  }

  public delete(action: string, params: Object = {}, headers: Object = {}) {
    return this.queries('DELETE', action, params, null, headers);
  }

  private handleError(error: Response | any) {
    let errMsg: any;
    const errorMessage = {
      'userMailMissing': 'Merci d\'indiquer une adresse e-mail valide.',
      'emailNotExist': 'Votre demande a été prise en compte. Vous pouvez maintenant vérifier votre boîte' +
      ' mail et réinitialiser votre mot de passe.',
      'limitAsk': 'Vous avez atteint le nombre maximum de tentatives autorisées. Veuillez contacter notre service' +
      ' technique.',
      'outdatedOrInvalidResetPasswordToken': 'Le jeton de réinitialisation du mot de passe est invalide ou a expiré',
      'userMailMissingOrInvalid': 'Les données de réinitialisation sont incorrectes',
      'SomethingWrong': 'Un problème est survenu merci de réessayer',
      'notFound': 'Pas de données disponibles',
    };

    const errorObj = error.json() || '';

    if (error.status === 401 && errorObj.code === 'invalidToken') {
      localStorage.clear();
      this.router.navigate(['users', 'login'], {queryParams: {msg: 'expToken'}});
    }else {
      errMsg = errorObj.code ? errorObj : {code: 'SomethingWrong', message: 'No orderLine Data'};
    }
    return Promise.reject(errorMessage[errMsg.code] || errorMessage[errMsg.code]);
  }
}
