import { Injectable } from '@angular/core';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import { TableColumn, DatatableComponent } from '@swimlane/ngx-datatable';
import { isDate } from 'util';

export type TableRowProcessor = (column: TableColumn, tableRow: any, reportRow: any) => boolean;

/*  This service is an "extension" to ngx-datatable whose scope does NOT
 *  include exporting data from its table as CSV file
 *
 *  Usage:
 *   1. import the service into the constructor of the component holding the datatable
 *   2. call the exportAsCSV method like this:
 *          this._exportCSVService.exportAsCSV(
 *               this.dataTable,
 *               title
 *           );
 *   3. (Optional) You may pass a third argument for processing rows (build your implementation with defaultDataProcessor
 *      as an example or wrap your own processor around it [keeping it for exceptions and processing])
 *          const processor: TableRowProcessor = (column: TableColumn, tableRow: any, reportRow: any): boolean => {
 *               const prop = column.prop;
 *               let skip = false;*
 *
 *               switch (prop) {
 *                  case <your sepcial column here>:
 *                       reportRow[prop] = ...
 *                       break;
 *                   default     :
 *                       skip = this._exportCSVService.defaultDataProcessor(column, tableRow, reportRow);
 *                       break;
 *              }
 *              return skip;
 *          }
 */
@Injectable()
export class ExportCSVService {
    // tslint:disable-next-line:max-line-length
    private regexDate = /^(-?(?:[1-9][0-9]*)?[0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9])T(2[0-3]|[01][0-9]):([0-5][0-9]):([0-5][0-9])(.[0-9]+)?(Z)?$/;

    constructor() { }

    /*  This is the default implementation for CSV export of a ngx-datatable
     *
     *  @column   : one of the column of the ngx-datatable
     *  @tableRow : a data row of the ngx-datatable
     *  @reportRow: the output (CSV) row corresponding to the tableRow
     *
     *  The boolean result value tells whether the row should be skipped (when true)
     */
    public defaultDataProcessor: TableRowProcessor = (column: TableColumn, tableRow: any, reportRow: any): boolean => {
        if (!column.name) { return false; }   // ignore column without name

        if (column.prop) {
            const prop = column.prop;

            if        (!prop || !tableRow[prop]) {
                reportRow[prop] = '';
            } else if (tableRow[prop] && this.regexDate.test(tableRow[prop])) {   // special processing for Date (stored as string)
                reportRow[prop] = new Date(tableRow[prop]).toLocaleDateString();
            } else {
                reportRow[prop] = tableRow[prop];
            }
        } else  {
            // special cases should be handled here
        }

        return false;
    }

    public exportAsCSV(dataTable: DatatableComponent, title?: string, dataProcessor: TableRowProcessor = this.defaultDataProcessor) {
        const columns: TableColumn[] = dataTable.columns || dataTable._internalColumns;
        const headers =
            columns
                .map((column: TableColumn) => column.name)
                .filter((e) => e);  // remove column without name (i.e. false value)

        const rows: any[] = dataTable.rows
            .map((tableRow) => {
                let skip = false;
                const reportRow = {};
                columns.forEach((column) => {
                    skip = skip || dataProcessor(column, tableRow, reportRow);
                })
                return !skip && reportRow;
            })
            .filter((e) => e);  // remove empty lines

        const options = {
            filename        : title,  // Wanna use the selector? -> this.elementRef.nativeElement.tagName.toLowerCase().replace('app-', ''),
            fieldSeparator  : ',',
            quoteStrings    : '"',
            decimalseparator: '.',
            showLabels      : true,
            headers         : headers,
            showTitle       : false,
            title           : title,
            useBom          : true,
        };
        return new Angular2Csv(rows, options.filename, options);
    }
}
