import { Injectable, Injector } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';
import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class AuthService {
  private apiUrl = environment.api.url + '/1/';
  private user: any | void = null;
  private userToken = null;
  headers: Headers;
  options: RequestOptions;


  constructor(
    private http: Http,
    private router: Router,
    private injector: Injector,
    private cookieService: CookieService) {
    this.userToken = localStorage.getItem('authToken');
  }

  isLogged() {
    return (this.userToken !== null && this.user !== null);
  }

  getToken() {
    return this.userToken;
  }

  getUser() {
    return this.user;
  }

  setUser(user: Object, userToken: string) {
    this.user = user;
    this.userToken = userToken;
    localStorage.setItem('authToken', userToken);
    localStorage.setItem('name', this.user.firstName + ' ' + this.user.lastName);
    localStorage.setItem('email', this.user.email);
    return this;
  }


  loginByToken() {
    this.headers = new Headers(
      { 'Content-Type': 'application/json',
        'access-token': this.getToken()
      });
    this.options = new RequestOptions({ headers: this.headers });
    return new Promise((resolve, reject) => {
      if (this.isLogged() === true) {
        resolve(true);
      } else if (this.getToken() !== null) {
        this.http.get(this.apiUrl + 'users/me', this.options).toPromise().then((res: Response) => {
          const result = res.json();
          if (result && !result.token) {
            resolve(false);
          } else {
            this.setUser(result, result.token);
            resolve(true);
          }
        }).catch(this.handleError);
      } else {
        resolve(false);
      }
    });
  }

  login(usercreds) {
    return this.http.post(this.apiUrl + 'auth/login', usercreds)
      .map((res: Response) => {
        const user = res.json();
        if (!user) {
          return false;
        }

        this.setUser(user, user.token);

        console.log('Setting PHPSESSID cookie:', user.PHPSESSID, 'on', document.domain);

        // TODO: (ADM-system only) remove when spaces have been split
        if (user.PHPSESSID) {
          this.cookieService.set('PHPSESSID', user.PHPSESSID, 0, '/', document.domain);
          this.cookieService.set('PHPSESSID', user.PHPSESSID, 0, '/', '.' + document.domain);  // weird behaviour around the domain
        }

        return true;
      }).catch(this.handleError);
  }

  private handleError(error: Response) {
    const errorMessage = {
      'authenticationFailed': 'Nom d\'utilisateur ou mot de passe invalide.',
      'userMailMissing': 'Nom d\'utilisateur ou mot de passe invalide.',
      'invalidToken': 'Oops, votre session a expiré!',
      'accountTemporarilyLocked': 'Votre compte est temporairement verrouillé, ' +
      'veuillez vérifier si vous êtes à l\'origine de ces tentatives de connexion.',
    };
    const errors = error.json();
    if (errors.code === 'invalidToken') {
      localStorage.removeItem('authToken');
      this.router.navigate(['users', 'login'], {queryParams: {msg: 'expToken'}});
    }
    return Observable.throw(errorMessage[errors.code] || 'Server error');
  }

  logOut() {
    localStorage.clear();

    // TODO: (ADM-system only) remove when spaces have been split
    if (this.cookieService.check('PHPSESSID')) {
      this.cookieService.delete('PHPSESSID', '/', document.domain);
      this.cookieService.delete('PHPSESSID', '/', '.' + document.domain);
    }

  }
}
