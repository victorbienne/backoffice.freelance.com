import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../services/index';
declare const $: any;
@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgotPassword.component.html',
  styleUrls: ['./forgotPassword.component.sass']
})
export class ForgotPasswordComponent implements OnInit {
  model = {
    userMail: ''
  };
  loading = false;
  public successMessage = '';
  public errorMessage = '';
  constructor(
    private _apiService: ApiService,
    private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
  }

  passToLogin() {
    this.router.navigate(['users', 'login']);
  }

  forgotPassword() {
    this.loading = true;
    this._apiService.post('users/forgot-password/ask', this.model).then((result) => {
      this.loading = false;
      $.notify({
        icon: '',
        message: 'Votre demande a été prise en compte. Vous pouvez maintenant vérifier votre ' +
        'boîte e-mail et réinitialiser votre mot de passe.'
      }, {
        type: 'success',
        delay: 6000,
        placement: {
          from: 'top',
          align: 'center',
        },
        template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0} text-center"' +
        ' role="alert">' +
        '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
        '<span data-notify="message">{2}</span>' +
        '</div>'
      });
    }).catch((error) => {
      this.loading = false;
      $.notify({
        icon: '',
        message: error
      }, {
        type: 'success',
        delay: 8000,
        placement: {
          from: 'top',
          align: 'center',
        },
        template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0} text-center"' +
        ' role="alert">' +
        '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
        '<span data-notify="message">{2}</span>' +
        '</div>'
      });
    })
  }

}
