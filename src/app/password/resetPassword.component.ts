import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../services/index';
declare const $: any;
export interface User {
  userMail: string;
  password: string;
  confirmPassword: string;
  token?: string;
}

@Component({
  selector: 'app-reset-password',
  templateUrl: './resetPassword.component.html',
  styleUrls: ['./resetPassword.component.sass']
})
export class ResetPasswordComponent implements OnInit, OnDestroy {
  public user: User;
  loading = false;
  private sub: any;
  public msgValidation = false;
  public hideForm = false;
  constructor(
    private _apiService: ApiService,
    private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.user = {
      userMail: '',
      password: '',
      token: '',
      confirmPassword: ''
    };
    this.sub = this.route
      .queryParams
      .subscribe(params => {
        this._apiService.get('users/forgot-password/check/' + params['userMail'] + '/' + params['token'] ).then((res) => {
          this.loading = false;
          this.hideForm = false;
          this.user.userMail = res.userMail;
          this.user.token = res.token;
        }).catch((error: Response) => {
          this.hideForm = true;
        });
      });
  }

  resetPassword() {
    this.loading = true;
    this._apiService.post('users/forgot-password/change', this.user).then((result) => {
      setTimeout(() => {
        this.loading = false;
        this.msgValidation = true;
        this.hideForm = true;
      }, 2000);
    }).catch((error) => {
      this.loading = false;
      this.msgValidation = false;
      $.notify({
        icon: '',
        message: error
      }, {
        type: 'danger',
        delay: 8000,
        placement: {
          from: 'top',
          align: 'center',
        },
        template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0} text-center"' +
        ' role="alert">' +
        '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
        '<span data-notify="message">{2}</span>' +
        '</div>'
      });
    })
  }

  passToLogin() {
    this.router.navigate(['users', 'login']);
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
