// Largely inspired from the ngClass directive:
//   https://github.com/angular/angular/blob/4.4.3/packages/common/src/directives/ng_class.ts#L10-L142
import { Directive, DoCheck, ElementRef, Input } from '@angular/core';

const RISK_COLORS = {
    'NONE'     : '#EDF0F5',
    'LOW'      : '#00ADEF',  // 'initial',
    'MEDIUM'   : '#62CB31',
    'HIGH'     : '#FFB606',
    'VERY_HIGH': '#E74C3C',
    'EXTREME'  : '#C0392B'
}

@Directive({
    selector: '[appRiskColor]'
})
export class RiskColorDirective implements DoCheck {
    // tslint:disable-next-line:no-input-rename
    @Input('appRiskColor') riskLevel: string;  // /!\ named input is required here!!

    public static getRiskLevelColor(level) {
        return RISK_COLORS[level];
    }

    constructor(private el: ElementRef) {
    }

    ngDoCheck(): void {
        this.el.nativeElement.style.color = RiskColorDirective.getRiskLevelColor(this.riskLevel);
    }
}
