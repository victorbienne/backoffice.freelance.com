import { Component, Input, HostBinding } from '@angular/core';
import { AuthService } from './services/index';
import { User } from './models/user';
import { environment } from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  @HostBinding('class.sidebar-mini') sideToogle = false;
  @HostBinding('class.nav-open') setRightClass = false;
  @Input() currentUser: User;
  public env = environment;

  constructor(private _authService: AuthService) {
    this.currentUser = this._authService.getUser();

    // Domain needs to be EXPLICITLY set on BOTH sides
    // document.domain = environment.domain;
  }
}
