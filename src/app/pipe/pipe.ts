import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeHtml, SafeStyle, SafeScript, SafeUrl, SafeResourceUrl } from '@angular/platform-browser';
import * as moment from 'moment/moment';

@Pipe({
    name: 'humanize'
})
// This type is a replacement for moment.humanize which
// is not precise enough
export class HumanizePipe implements PipeTransform {
    transform(value: number, args): string {
        const localeDataRel: {
            y: any,
            yy: any,
            M: any,
            MM: any,
            d: any,
            dd: any
        }  = moment.localeData()['_relativeTime'];  // Trick to access "private" data from moment

        let humanDuration = '';
        let timeStruc: any = { years : 0,
                               months: 0,
                               days  : 0} ;

        timeStruc     = moment.duration(value)['_data'];
        humanDuration = ((timeStruc.years === 1)  ? localeDataRel.y
                                                  : (timeStruc.years > 1)  ? localeDataRel.yy.replace('%d', timeStruc.years) : '')
                        + ' ' +
                        ((timeStruc.months === 1) ? localeDataRel.M
                                                  : (timeStruc.months > 1) ? localeDataRel.MM.replace('%d', timeStruc.months) : '')
                        + ' ' +
                        ((timeStruc.days === 1)   ? localeDataRel.d
                                                  : (timeStruc.days > 1)   ? localeDataRel.dd.replace('%d', timeStruc.days) : '');

        humanDuration = humanDuration.trim().replace(/\s+/g, ' ');

        return(humanDuration);
    }
}

@Pipe({
  name: 'truncate'
})
export class TruncatePipe implements PipeTransform {
  transform(value: string, length: number): string {
    if (!value) {
      return value;
    }

    const limit = parseInt(length + '', 10);
    return value.length > limit ? value.substring(0, limit) + '...' : value;
  }
}

// See: https://forum.ionicframework.com/t/inserting-html-via-angular-2-use-of-domsanitizationservice-bypasssecuritytrusthtml/62562/2
@Pipe({
    name: 'safe'
})
export class SafePipe implements PipeTransform {

    constructor(protected _sanitizer: DomSanitizer) {

    }

    public transform(value: string, type: string): SafeHtml | SafeStyle | SafeScript | SafeUrl | SafeResourceUrl {
        switch (type) {
            case 'html'       : return this._sanitizer.bypassSecurityTrustHtml(value);
            case 'style'      : return this._sanitizer.bypassSecurityTrustStyle(value);
            case 'script'     : return this._sanitizer.bypassSecurityTrustScript(value);
            case 'url'        : return this._sanitizer.bypassSecurityTrustUrl(value);
            case 'resourceUrl': return this._sanitizer.bypassSecurityTrustResourceUrl(value);
            default: throw new Error(`Unable to bypass security for invalid type: ${type}`);
        }
    }
}
