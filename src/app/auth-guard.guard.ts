import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AuthService } from './services/index';

@Injectable()
export class AuthGuardGuard implements CanActivate {
  constructor(private router: Router, private _authService: AuthService) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this._authService.loginByToken().then((logged) => {
      if (logged === false) {
        this.router.navigate(['users', 'login'], { queryParams: { returnUrl: state.url }});
        return false;
      }
      return true;
    });
  }
}
