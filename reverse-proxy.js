var http      = require('http');
var httpProxy = require('http-proxy');

//
// netsh on Windows
//
// Usage: 
//   netsh interface portproxy add v4tov4 listenport=80 listenaddress=127.12.34.56 connectport=8001 connectaddress=127.0.0.1
//

// More complex routes?
//   see: https://github.com/donasaur/http-proxy-rules
//        https://coderwall.com/p/gc49uq/default-host-with-node-http-proxy
const PROXY_OPTIONS = {
    target: {
        host: 'localhost',
        port: 4300
      }
};
console.log("listening on port 8001");
httpProxy.createProxyServer(PROXY_OPTIONS).listen(8001);


//
// Create your proxy server for custom logic
//
// var proxy = httpProxy.createProxyServer({});

//
// Create your custom server and just call `proxy.web()` to proxy
// a web request to the target passed in the options
// also you can use `proxy.ws()` to proxy a websockets request
//
// var server = http.createServer(function(req, res) {
//     console.log(req.url)
//     // You can define here your custom logic to handle the request
//     // and then proxy the request.
//     if (req.url.indexOf('/adm-system.localhost') >= 0) {
//         console.log(req);
//         proxy.web(req, res, { target: 'http://espaceconsultanttest' });        
//     }
//     else {
//         proxy.web(req, res, { target: 'http://localhost:4300' });
//     }
// });

// console.log("listening on port 8001")
// server.listen(8001);
