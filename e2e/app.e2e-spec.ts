import { AppPortagePage } from './app.po';

describe('app-portage App', () => {
  let page: AppPortagePage;

  beforeEach(() => {
    page = new AppPortagePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
